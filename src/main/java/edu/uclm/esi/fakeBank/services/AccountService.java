package edu.uclm.esi.fakeBank.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.uclm.esi.fakeBank.dao.AccountRepository;
import edu.uclm.esi.fakeBank.dao.CounterRepository;
import edu.uclm.esi.fakeBank.model.Account;
import edu.uclm.esi.fakeBank.model.Counter;

@Service
public class AccountService {
	
	@Autowired
	private CounterRepository counterDAO;
	
	@Autowired
	private AccountRepository accountDAO;
	
	public static final String IBAN = "XY25";
	public static final String BANK = "0999";
	public static final String OFFICE = "0001";
	
	public Account createAccount() {
		Account acc = new Account();
		acc.setIban(IBAN);
		acc.setBank(BANK);
		acc.setOffice(OFFICE);
		Integer number = counterDAO.getLast("ACCOUNT");
		if (number==null) {
			Counter counter = new Counter();
			counter.setName("ACCOUNT");
			counter.setLastNumber(1);
			counterDAO.save(counter);
		}
		acc.setNumber(number);
		acc.setControl(this.calculateControl(acc));
		accountDAO.save(acc);
		return acc;
	}

	private String calculateControl(Account acc) {
		int digito1[] = new int[8];
		int i = 0;
		digito1[i] = Integer.parseInt("" + acc.getBank().charAt(i++))*4;
		digito1[i] = Integer.parseInt("" + acc.getBank().charAt(i++))*8;
		digito1[i] = Integer.parseInt("" + acc.getBank().charAt(i++))*5;
		digito1[i] = Integer.parseInt("" + acc.getBank().charAt(i++))*10;
		
		i= 0;
		digito1[i+4] = Integer.parseInt("" + acc.getOffice().charAt(i++))*9;
		digito1[i+4] = Integer.parseInt("" + acc.getOffice().charAt(i++))*7;		
		digito1[i+4] = Integer.parseInt("" + acc.getOffice().charAt(i++))*3;
		digito1[i+4] = Integer.parseInt("" + acc.getOffice().charAt(i++))*6;
		
		int dc1 = 0;
		for (int j=0; j<digito1.length; j++)
			dc1=dc1 + digito1[j];
		dc1 = dc1 % 11;
		dc1 = 11-dc1;
		if (dc1==10)
			dc1 = 1;
		else if (dc1==11)
			dc1 = 0;
		
		int dc2 = 0;
		int[] multiplicadores = { 1, 2, 4, 8, 5, 10, 9, 7, 3, 6 };
		for (int j=0; j<acc.getNumber().length(); j++)
			dc2 = dc2 + (Integer.parseInt("" + acc.getNumber().charAt(j)) * multiplicadores[j]);
		dc2 = dc2%11;
		dc2 = 11-dc2;
		if (dc2==10)
			dc2 = 1;
		else if (dc2==11)
			dc2 = 0;
	
		String result = "" + dc1 + dc2;
		return result;
	}
}
