package edu.uclm.esi.fakeBank.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.uclm.esi.fakeBank.model.Login;

public interface LoginRepository extends JpaRepository <Login, String> {

}
