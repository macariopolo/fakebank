package edu.uclm.esi.fakeBank.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import edu.uclm.esi.fakeBank.model.Counter;

public interface CounterRepository extends JpaRepository <Counter, String> {

	@Query(value = "select max(last_number) from counter where name= :elementName", nativeQuery = true)
	Integer getLast(@Param("elementName") String name);

}
