package edu.uclm.esi.fakeBank.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Account {
	@Id
	@Column(length = 36)
	private String id;
	@Column(length = 4)
	private String iban, bank, office;
	@Column(length = 2)
	private String control;
	@Column(length = 10)
	private String number;
	@ManyToOne
	private User user;
	
	public Account() {
		this.id = UUID.randomUUID().toString();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getOffice() {
		return office;
	}

	public void setOffice(String office) {
		this.office = office;
	}

	public String getControl() {
		return control;
	}

	public void setControl(String control) {
		this.control = control;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setNumber(Integer number) {
		if (number==null)
			number=1;
		this.number = "" + number;
		int leftDigits = 10 - this.number.length();
		for (int i=0; i<leftDigits; i++)
			this.number = "0" + this.number;
	}
	
}
