function doGet(url, callback, error, async) {
	var data = {
		url : url,
		type : "GET",
		contentType: 'application/json',
		success : callback,
		error : error
	};
	if (async==undefined)
		data.async = true;
	else
		data.async = async;
	$.ajax(data);
}

function doPost(url, info, callback, error) {
	var data = {
		data : JSON.stringify(info),
		url : url,
		type : "POST",
		contentType: 'application/json',
		success : callback,
		error : error
	};
	$.ajax(data);		
}

function doPut(url, info, callback, error) {
	var data = {
		data : JSON.stringify(info),
		url : url,
		type : "PUT",
		contentType: 'application/json',
		success : callback,
		error : error
	};
	$.ajax(data);		
}

function doDelete(url, info, callback, error) {
	var data = {
		data : JSON.stringify(info),
		url : url,
		type : "DELETE",
		contentType: 'application/json',
		success : callback,
		error : error
	};
	$.ajax(data);		
}