package edu.uclm.esi.fakeBank.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Counter {
	@Id
	@Column(length = 15)
	private String name;
	private Integer lastNumber;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Integer getLastNumber() {
		return lastNumber;
	}
	
	public void setLastNumber(Integer lastNumber) {
		this.lastNumber = lastNumber;
	}
}
