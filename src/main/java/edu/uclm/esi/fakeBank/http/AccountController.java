package edu.uclm.esi.fakeBank.http;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import edu.uclm.esi.fakeBank.model.Account;
import edu.uclm.esi.fakeBank.model.User;
import edu.uclm.esi.fakeBank.services.AccountService;

@RestController
@RequestMapping("accounts")
public class AccountController extends CookiesController {
	
	@Autowired
	private AccountService accountService;
	
	@GetMapping("/getAccounts")
	@ResponseBody
	public List<String> getAccounts(HttpSession session) {
		User user = (User) session.getAttribute("user");
		return Manager.get().getAccountsRepository().findByUserId(user.getId());
	}

	@GetMapping("/createAccount")
	@ResponseBody
	public Account createAccount(HttpSession session) {
		Account acc = accountService.createAccount();
		return acc;
	}
}
