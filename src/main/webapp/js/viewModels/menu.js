define([ 'knockout', 'appController', 'ojs/ojmodule-element-utils', 'accUtils',
		'jquery' ], function(ko, app, moduleUtils, accUtils, $) {

	class MenuViewModel extends SecurityViewModel {
		constructor() {
			super(ko);
			let self = this;
						
			// Header Config
			self.headerConfig = ko.observable({
				'view' : [],
				'viewModel' : null
			});
			moduleUtils.createView({
				'viewPath' : 'views/header.html'
			}).then(function(view) {
				self.headerConfig({
					'view' : view,
					'viewModel' : app.getHeaderModel()
				})
			});

			self.accounts = ko.observableArray([]);
			self.newAccount = ko.observable(null); 
		}

		connected() {
			document.title = "Menu";

			let self = this;
			doGet(
				"accounts/getAccounts",
				function(response) {
					self.accounts(response);
				},
				function(response) {
					self.error(response.responseJSON.message);
				}
			);
		};

		createAccount() {
			let self = this;
			doGet(
				"accounts/createAccount",
				function(response) {
					self.newAccount(response);
				}, 
				function(response) {
					self.error(response.responseJSON.message);
				}
			);
		}
	}

	return MenuViewModel;
});
