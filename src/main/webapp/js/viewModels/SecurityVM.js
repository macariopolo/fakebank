class SecurityViewModel {
	constructor(ko) {
		let self = this;
					
		self.userName = ko.observable(null);
		self.error = ko.observable(null);

		doGet(
			"user/checkUser",
			function(response) {
				self.userName(response);
			},
			function(response) {
				self.error(response.responseJSON.errorMessage);
			}
		);			
	}
}
