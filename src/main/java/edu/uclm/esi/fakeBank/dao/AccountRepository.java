package edu.uclm.esi.fakeBank.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.uclm.esi.fakeBank.model.Account;

public interface AccountRepository extends JpaRepository <Account, String> {

	List<String> findByUserId(String userId);

}
