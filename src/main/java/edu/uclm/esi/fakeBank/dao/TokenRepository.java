package edu.uclm.esi.fakeBank.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.uclm.esi.fakeBank.model.Token;

public interface TokenRepository extends JpaRepository<Token, String> {

}
